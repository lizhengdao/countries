package com.example.projekat.utils;

import com.example.projekat.models.Currency;
import java.util.List;

public class ListStringBuilder {
    public static String buildLangString(List<String> languages) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < languages.size(); i++) {
            if (i < languages.size() - 2) {
                sb.append(languages.get(i));
                sb.append(", ");
            } else if (i == languages.size() - 1) {
                sb.append(languages.get(i));
            }
        }

        return sb.toString();
    }

    public static String buildCurrString(List<Currency> currencies) {
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < currencies.size(); i++) {
            if (i < currencies.size() - 2) {
                sb.append(currencies.get(i).getName());
                sb.append(", ");
            } else if (i == currencies.size() - 1) {
                sb.append(currencies.get(i).getName());
            }
        }

        return sb.toString();
    }
}
