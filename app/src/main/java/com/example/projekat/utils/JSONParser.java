package com.example.projekat.utils;

import android.util.Log;

import com.example.projekat.models.Country;
import com.example.projekat.models.Currency;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JSONParser {

    public static Country parseJSONObject(JSONObject object) throws Exception {
        Country country = new Country();

        if(object.has("name")) {
            country.setName(object.getString("name"));
        }
        if(object.has("capital")) {
            country.setCapital(object.getString("capital"));
        }
        if(object.has("population")) {
            country.setPopulation(object.getLong("population"));
        }
        if(object.has("flag")) {
//            URL url = new URL(object.getString("flag"));
//            country.setFlag(BitmapFactory.decodeStream(url.openConnection().getInputStream()));
            country.setFlag(object.getString("flag"));
        }
        if(object.has("languages")) {
            JSONArray arr = new JSONArray(object.getString("languages"));
            List<String> lang = new ArrayList<>();

            for(int i = 0; i < arr.length(); i++ ) {
                JSONObject obj = arr.getJSONObject(i);
                if(obj.has("name")) {
                    lang.add(obj.getString("name"));
                }
            }

            country.setLanguages(lang);
        }

        if(object.has("region")) {
            country.setRegion(object.getString("region"));
        }
        if(object.has("numericCode")) {
            country.setNumericCode(object.getString("numericCode"));
        }

        if(object.has("currencies")) {
            JSONArray array = new JSONArray(object.getString("currencies"));
            List<Currency> curr = new ArrayList<>();

            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                Currency c = new Currency();

                if (obj.has("code")) {
                    c.setCode(obj.getString("code"));
                }
                if (obj.has("symbol")) {
                    c.setSymbol(obj.getString("symbol"));
                }
                if (obj.has("name")) {
                    c.setName(obj.getString("name"));
                }

                curr.add(c);
            }

            country.setCurrencies(curr);
        }

        if(object.has("area")) {
            country.setArea(object.getDouble("area"));
        }

        return country;
    }

    public static List<Country> parseJSONArray(JSONArray array) {
        List<Country> countries = new ArrayList<>();

        for(int i = 0; i < array.length(); i++) {
            try {
                countries.add(parseJSONObject(array.getJSONObject(i)));
            } catch (Exception e) {
                Log.e("PARSING_ERROR", "Niz nije parsiran");
            }
        }

        return countries;

    }

}
