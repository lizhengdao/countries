package com.example.projekat.ui.search;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class SearchViewModel extends AndroidViewModel {

    private MutableLiveData<JSONArray> results;
    private MutableLiveData<String> query;

    public MutableLiveData<JSONArray> getResults() {
        return results;
    }

    public MutableLiveData<String> getQuery() {
        return query;
    }

    public SearchViewModel(@NonNull Application application) {
        super(application);

        results = new MutableLiveData<>();
        query = new MutableLiveData<>();
    }

    public void doSearch(String query) {
            // Zahtjev
            RequestQueue requestQueue = Volley.newRequestQueue(getApplication().getApplicationContext());

            String url = "https://restcountries.eu/rest/v2/name/" + query.toLowerCase();

            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    results.setValue(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        results.setValue(new JSONArray("[]"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            requestQueue.add(jsonArrayRequest);
    }



}
