package com.example.projekat.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.projekat.MainViewModel;
import com.example.projekat.R;
import com.google.firebase.auth.FirebaseAuth;

public class LoginFragment extends Fragment implements View.OnClickListener{

    private LoginViewModel viewModel;
    private Button btnLogin;
    private Button btnRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private TextView labelError;
    private NavController navController;
    private MainViewModel mainViewModel;
    private ProgressBar progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Login View model sadrzi stanje korisnika prilikom autentitikacije
        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        // Main View model sadrzi globalno stanje aplikacije da li je korisnik vec prijavljen ili ne
        mainViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);

        //Initicjalizacija
        btnLogin = root.findViewById(R.id.btn_main_login);
        btnRegister = root.findViewById(R.id.btn_main_signup);
        inputEmail = root.findViewById(R.id.input_main_email);
        inputPassword = root.findViewById(R.id.input_main_password);
        labelError = root.findViewById(R.id.label_main_error);
        progressBar = root.findViewById(R.id.loginProgressBar);

        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

        return root;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Navigacija za kretanje kroz login, signup i ka home
        navController = Navigation.findNavController(view);

        //Observe login viewmodel za promjene
        viewModel.getAuthenticationState().observe(getViewLifecycleOwner(), new Observer<AuthenticationState>() {
            @Override
            public void onChanged(AuthenticationState authenticationState) {
                // Skloniti progres bar
                progressBar.setVisibility(View.GONE);
                switch (authenticationState) {
                    case AUTHENTICATION_SUCCESS:
                        // Korisnik uspjesno prijavljen, otici na home i ukloniti sve sa steka
                        mainViewModel.getUser().setValue(FirebaseAuth.getInstance().getCurrentUser());
                        navController.popBackStack(R.id.nav_home, false);
                        break;
                    case AUTHENTICATION_FAILED:
                        labelError.setText(R.string.label_error_msg);
                        break;
                    case INVALID_EMPTY_FIELDS:
                        labelError.setText(R.string.label_error_msg_empty);
                        break;
                }
            }
        });
        // Ako korisnik pritisne back button, izadji iz aplikacije
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(),
                new OnBackPressedCallback(true) {
                    @Override
                    public void handleOnBackPressed() {
                        Intent a = new Intent(Intent.ACTION_MAIN);
                        a.addCategory(Intent.CATEGORY_HOME);
                        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(a);
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        // Skloniti obavjestenja ukoliko korisnik ode na register i vrati se
        labelError.setText("");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_main_signup:
                // Dodati na stek register fragment
                navController.navigate(R.id.action_loginFragment_to_registerFragment);
                break;
            case R.id.btn_main_login:
                signIn();
                break;
        }
    }

    private void signIn() {
        // Ukloniti sva obavjestenja kada korisnik pokusa da se prijavi
        labelError.setText("");
        // Prikazati progres
        progressBar.setVisibility(View.VISIBLE);
        // Izvrsiti autentikaciju
        viewModel.authenticate(inputEmail.getText().toString(), inputPassword.getText().toString());
    }

}
