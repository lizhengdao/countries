package com.example.projekat.ui.country;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.ahmadrosid.svgloader.SvgLoader;
import com.example.projekat.MainViewModel;
import com.example.projekat.R;
import com.example.projekat.models.Country;
import com.example.projekat.utils.ListStringBuilder;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CountryFragment extends Fragment {

    private MainViewModel mainViewModel;
    private CountryViewModel countryViewModel;
    private ImageView imgFlag;
    private TextView countryDesc;
    private TextView countryName;
    private TextView countryCapital;
    private TextView countryPopulation;
    private ProgressBar progressBar;
    private FirebaseFirestore fStore;
    private ListView languages;
    private MenuItemState menuState;
    private FirebaseUser user;
    private Country currentCountry;
    private boolean isCountryAdded;

    enum MenuItemState{
        FILLED, EMPTY
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mainViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        countryViewModel = new ViewModelProvider(this).get(CountryViewModel.class);

        fStore = FirebaseFirestore.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_country, container, false);
        // Init
        imgFlag = root.findViewById(R.id.img_country_flag);
        countryDesc = root.findViewById(R.id.label_country_desc);
        countryName = root.findViewById(R.id.label_country_country_name);
        countryCapital = root.findViewById(R.id.label_country_country_capital);
        countryPopulation = root.findViewById(R.id.label_country_country_population);
        progressBar = root.findViewById(R.id.fragment_country_progress_bar);
        languages = root.findViewById(R.id.langauges_list_view);

        // Osluskivanje da li je postavljena drzava kada je kliknuta
        // Preuzimaju se detalji drzave
        mainViewModel.getCountryName().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                countryViewModel.loadData(s);
            }
        });

        // Osluskivanje kada je zavrseno preuzimanje detalja
        countryViewModel.getCountry().observe(getViewLifecycleOwner(), new Observer<Country>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onChanged(Country country) {
                progressBar.setVisibility(View.GONE);
                // SVG plugin
                SvgLoader.pluck().with(requireActivity()).load(country.getFlag(), imgFlag);
                countryName.setText(country.getName());
                countryCapital.setText(getString(R.string.placeholder_country_capital_city, country.getCapital()));
                countryPopulation.setText(getString(R.string.placeholder_country_population, country.getPopulation()));

                String lang = ListStringBuilder.buildLangString(country.getLanguages());
                String curr = ListStringBuilder.buildCurrString(country.getCurrencies());
                NumberFormat nf = NumberFormat.getNumberInstance(Locale.ITALY);

                countryDesc.setText(country.getName() + " is located in " + country.getRegion()
                        + " region and spans over " + nf.format(country.getArea())
                        + " square kilometers. Capital city is " + country.getCapital()
                        + " and the country has population around " + nf.format(country.getPopulation()) +
                        " people. Official languages are: " + lang +
                        ". While there you can you use one of these currencies: " + curr + ".");


                currentCountry = country;

                checkIfCountryIsAdded();

            }
        });

        return root;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_to_favorites, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.action_favorites) {
            handleClick();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (menuState == MenuItemState.FILLED) {
            menu.getItem(0).setIcon(R.drawable.ic_star_fill);
        } else if (menuState == MenuItemState.EMPTY) {
            menu.getItem(0).setIcon(R.drawable.ic_star_empty);
        }
    }

    private void setIsCountryAdded(boolean isCountryAdded) {
        this.isCountryAdded = isCountryAdded;
    }

    private void checkIfCountryIsAdded() {
        Log.e("A", "CHECKING");

        // Pretraga firebase
        fStore.collection("users").whereEqualTo("userId", user.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    // Ako postoji drzava
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String itemName = (String) document.get("name");
                        if (itemName.equals(currentCountry.getName())) {
                            Log.e("A", "SET NA TRUE");
                            setIsCountryAdded(true);
                            break;
                        }
                    }

                    if (isCountryAdded) {
                        menuState = MenuItemState.FILLED;
                        getActivity().invalidateOptionsMenu();
                    } else {
                        menuState = MenuItemState.EMPTY;
                        getActivity().invalidateOptionsMenu();
                    }
                }
            }
        });
    }

    private void handleClick() {

        if (currentCountry != null) {

            final Map<String, String> dataToAdd= new HashMap<>();

            dataToAdd.put("name", currentCountry.getName());
            dataToAdd.put("region", currentCountry.getRegion());
            dataToAdd.put("capital", currentCountry.getCapital());
            dataToAdd.put("flag", currentCountry.getFlag());
            dataToAdd.put("userId", user.getUid());
            // Provjeriti da li se drzava vec nalazi u favoritima
            if (!isCountryAdded) {
                addToCollection(dataToAdd);
            } else {
                //Pronadji drzavu i ukloni je
                fStore.collection("users").whereEqualTo("userId", user.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (document.get("name").equals(currentCountry.getName())) {
                                    removeCountryFromFavorites(document.getId());
                                }
                            }
                        }
                    }
                });
            }
        }
    }

    private void removeCountryFromFavorites(String countryId) {

        fStore.collection("users").document(countryId).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                menuState = MenuItemState.EMPTY;
                Activity activity = getActivity();
                activity.invalidateOptionsMenu();
                setIsCountryAdded(false);
                if (activity != null && isAdded()) {
                    Snackbar.make(requireActivity().findViewById(R.id.main_coordinator_layout), "Removed from favorites", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addToCollection(Map<String, String> toAdd) {
        final Activity activity = getActivity();

        fStore.collection("users").add(toAdd).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                menuState = MenuItemState.FILLED;
                activity.invalidateOptionsMenu();
                setIsCountryAdded(true);
                if(activity != null && isAdded()) {
                    Snackbar.make(activity.findViewById(R.id.main_coordinator_layout), "Saved to favorites", Snackbar.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (activity != null && isAdded()) {
                    Snackbar.make(activity.findViewById(R.id.main_coordinator_layout), "Something went wrong", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

}
