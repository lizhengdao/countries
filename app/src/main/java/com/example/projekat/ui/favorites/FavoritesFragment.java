package com.example.projekat.ui.favorites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projekat.MainViewModel;
import com.example.projekat.R;
import com.example.projekat.models.Country;
import com.example.projekat.ui.home.CountryListAdapter;
import com.example.projekat.utils.JSONParser;

import org.json.JSONArray;

import java.util.List;

public class FavoritesFragment extends Fragment {

    private FavoritesViewModel favoritesViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private MainViewModel mainViewModel;
    private ProgressBar progressBar;
    private TextView labelNoData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        favoritesViewModel = new ViewModelProvider(this).get(FavoritesViewModel.class);
        // Main viewmodel sadrzi ime drzave na koju je kliknuto
        mainViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_favorites, container, false);

        // Init
        recyclerView = root.findViewById(R.id.fragment_favorites);
        layoutManager = new LinearLayoutManager(requireActivity());
        recyclerView.setLayoutManager(layoutManager);

        progressBar = root.findViewById(R.id.favoritesProgressBar);
        labelNoData = root.findViewById(R.id.label_favorites_no_data);

        // Slusanje kada ce se dopremiti podaci o favorite countries
        favoritesViewModel.getCountries().observe(getViewLifecycleOwner(), new Observer<JSONArray>() {
            @Override
            public void onChanged(JSONArray jsonArray) {
                // Skloniti progres bar
                progressBar.setVisibility(View.GONE);
                if (labelNoData.getVisibility() == View.VISIBLE) {
                    labelNoData.setVisibility(View.GONE);
                }
                // Ako ima podataka prikazi ih
                if (jsonArray != null) {
                    showData(jsonArray);
                } else {
                    labelNoData.setVisibility(View.VISIBLE);
                }
            }
        });

        return root;
    }

    private void showData(JSONArray array) {
        // Parsiranje dopremljenih podataka i postavljanje u view
        List<Country> countries = JSONParser.parseJSONArray(array);
        adapter = new CountryListAdapter(countries, requireActivity(), mainViewModel);
        recyclerView.setAdapter(adapter);
    }


}
