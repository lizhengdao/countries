package com.example.projekat.ui.settings;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.projekat.MainViewModel;
import com.example.projekat.R;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class SettingsFragment extends Fragment implements View.OnClickListener{

    private SettingsViewModel mViewModel;
    private Button btnLogout;
    private MainViewModel mainViewModel;
    private Button btnUploadPic;
    private FirebaseUser user;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        user = FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        btnLogout = root.findViewById(R.id.btn_logout);
        btnUploadPic = root.findViewById(R.id.btn_upload_pic);

        btnLogout.setOnClickListener(this);
        btnUploadPic.setOnClickListener(this);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void signOut() {
        AuthUI.getInstance().signOut(requireActivity().getApplicationContext()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mainViewModel.getUser().setValue(null);
            }
        });
    }

    private void uploadProfilePicture() {

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && data != null) {
            Uri imageUri = data.getData();
            try {
                InputStream imageStream = requireActivity().getContentResolver().openInputStream(imageUri);
                Bitmap imageBitmap = BitmapFactory.decodeStream(imageStream);
                Bitmap resized = Bitmap.createScaledBitmap(imageBitmap, 128, 128, true);
                uploadImageAndSaveUri(resized);
            } catch (Exception e) {}

        }
    }

    private void uploadImageAndSaveUri(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] data = baos.toByteArray();

        StorageReference storageRef = FirebaseStorage.getInstance().getReference().child("images/" + user.getUid());

        UploadTask uploadTask = storageRef.putBytes(data);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    // Handle
                }

                return storageRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    changeProfileAvatar(task.getResult(), image);
                } else {
                    // Handle
                }
            }
        });
    }

    private void changeProfileAvatar(Uri imageUri, Bitmap imageBitMap) {

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setPhotoUri(imageUri).build();
        user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                String text;
                if (task.isSuccessful()) {
                    text = "Picture updated";
                } else {
                    text = "Sorry something went wrong";
                }
                Activity activity = getActivity();

                if (activity != null && isAdded()) {
                    Snackbar.make(activity.findViewById(R.id.main_coordinator_layout), text, Snackbar.LENGTH_SHORT).show();
                }

                NavigationView  navigationView = requireActivity().findViewById(R.id.nav_view);
                View header = navigationView.getHeaderView(0);

                ImageView imageAvatar = header.findViewById(R.id.nav_header_avatar);
                imageAvatar.setImageBitmap(imageBitMap);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_logout:
                signOut();
                break;
            case R.id.btn_upload_pic:
                uploadProfilePicture();
                break;
        }
    }
}
