package com.example.projekat.ui.login;

public enum AuthenticationState {
    INVALID_EMPTY_FIELDS,
    AUTHENTICATION_FAILED,
    AUTHENTICATION_SUCCESS,
}
