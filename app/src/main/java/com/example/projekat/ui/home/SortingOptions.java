package com.example.projekat.ui.home;

import androidx.annotation.NonNull;

public enum SortingOptions {
    NAME, REGION, MOST_INHABITED;


    @NonNull
    @Override
    public String toString() {

        switch (this) {
            case NAME:
                return "By Name";
            case REGION:
                return "By Region";
            case MOST_INHABITED:
                return "Most Inhabited";
        }

        return "";
    }
}

