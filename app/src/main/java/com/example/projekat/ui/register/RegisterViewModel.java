package com.example.projekat.ui.register;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class RegisterViewModel extends ViewModel {

    // Cuvamo stanje korisnika prilikom registracije
    private MutableLiveData<RegistrationState> registrationState;
    private FirebaseAuth fAuth;

    MutableLiveData<RegistrationState> getRegistrationState() {
        return registrationState;
    }

    public RegisterViewModel() {
        registrationState = new MutableLiveData<>();
        fAuth = FirebaseAuth.getInstance();
    }

    private boolean areCredentialsProvided(String username, String email, String password) {
        return !username.equals("") && !email.equals("") && !password.equals("");
    }

    void createAccount(final String username, String email, String password) {
        // Ako su svi podaci unijeti kreiraj novi nalog
        if (areCredentialsProvided(username, email, password)) {
            fAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Ako je kreiranje naloga uspjesno uzmi korisnika i postavi mu koriscniko ime
                        FirebaseUser user = fAuth.getCurrentUser();

                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(username).build();

                        user.updateProfile(profileUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                // Stanje registracije je uspjesno, koristi se da update view model iz main activity
                                registrationState.setValue(RegistrationState.REGISTRATION_SUCCESS);
                            }
                        });
                    } else {
                        // Hvatamo greske od strane FirebaseAuth API ako korisnik nije zadovoljio uslove registracije
                        try {
                            throw task.getException();
                        } catch (FirebaseAuthWeakPasswordException e) {
                            registrationState.setValue(RegistrationState.INVALID_PASSWORD);
                        } catch (FirebaseAuthUserCollisionException e) {
                            registrationState.setValue(RegistrationState.USER_ALREADY_EXISTS);
                        } catch (FirebaseAuthInvalidCredentialsException e) {
                            registrationState.setValue(RegistrationState.INVALID_EMAIL);
                        } catch (Exception e) {
                            registrationState.setValue(RegistrationState.REGISTRATION_FAILED);
                        }
                    }
                }
            });
        } else {
            registrationState.setValue(RegistrationState.INVALID_EMPTY_FIELDS);
        }
    }
}
