package com.example.projekat.ui.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.projekat.models.Country;
import com.example.projekat.utils.JSONParser;

import org.json.JSONArray;

import java.util.List;

// AndroidViewModel jer nam treba context za Volley
public class HomeViewModel extends AndroidViewModel {

    private MutableLiveData<List<Country>> countries;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        countries = new MutableLiveData<>();
        fetchData();
    }


    public MutableLiveData<List<Country>> getCountries() { return countries; }

    private void fetchData() {
        //Radi na main thread
        RequestQueue queue = Volley.newRequestQueue(getApplication().getApplicationContext());
        //URL ka api
        String url = "https://restcountries.eu/rest/v2/all?fields=name;capital;population;languages;flag;region;numericCode;currencies;area";
        //GET request
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                countries.setValue(JSONParser.parseJSONArray(response));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    countries.setValue(null);
                } catch (Exception e) {};
            }
        });
        //Zapocni request
        queue.add(jsonArrayRequest);
    }



}