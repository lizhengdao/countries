package com.example.projekat.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projekat.MainViewModel;
import com.example.projekat.R;
import com.example.projekat.models.Country;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private MainViewModel mainViewModel;
    private ProgressBar progressBar;
    private TextView lableNoData;
    private Spinner spinnerSortHome;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        // Main view model sluzi da nam kaze da li se korisnik ulogovao
        mainViewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        // Inicijalizacija
        lableNoData = root.findViewById(R.id.label_home_no_data);
        spinnerSortHome =  root.findViewById(R.id.spinner_sort_home);
        recyclerView = root.findViewById(R.id.fragment_home);
        layoutManager = new LinearLayoutManager(requireActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setVisibility(View.GONE);

        List<String> sortingOptions = new ArrayList<String>() {{
            add(SortingOptions.NAME.toString());
            add(SortingOptions.REGION.toString());
            add(SortingOptions.MOST_INHABITED.toString());
        }};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_dropdown_item, sortingOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSortHome.setAdapter(adapter);

        progressBar = root.findViewById(R.id.homeProgressBar);

        // Osluskivanje da li je korisnik ulogovan
        homeViewModel.getCountries().observe(getViewLifecycleOwner(), countries -> {
            // Kada se korisnik uloguje pocinje preuzimanje drzava
            if (lableNoData.getVisibility() == View.VISIBLE) {
                lableNoData.setVisibility(View.GONE);
            }
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            showData(countries);
        });

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinnerSortHome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getItemAtPosition(position);
                String value = item.toString();
                SortingOptions option;
                switch (value.toLowerCase()) {
                    case "by name":
                        option = SortingOptions.NAME;
                        break;
                    case "by region":
                        option = SortingOptions.REGION;
                        break;
                    case "most inhabited":
                        option = SortingOptions.MOST_INHABITED;
                        break;
                    default:
                        option = SortingOptions.NAME;
                        break;
                }

                sortData(option);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void sortData(SortingOptions option) {
        switch (option) {
            case NAME:
                sortByName();
                break;
            case REGION:
                sortByRegion();
                break;
            case MOST_INHABITED:
                sortByPopulation();
                break;
        }
    }

    private void sortByName() {
        MutableLiveData<List<Country>> countries = homeViewModel.getCountries();

        if (countries.getValue() != null) {
            Collections.sort(countries.getValue(), (c1, c2) -> c1.getName().compareTo(c2.getName()));
            countries.setValue(countries.getValue());
        }
    }

    private void sortByRegion() {
        MutableLiveData<List<Country>> countries = homeViewModel.getCountries();

        if (countries.getValue() != null) {
            Collections.sort(countries.getValue(), (c1, c2) -> c1.getRegion().compareTo(c2.getRegion()));
            countries.setValue(countries.getValue());
        }
    }

    private void sortByPopulation() {
        MutableLiveData<List<Country>> countries = homeViewModel.getCountries();

        if (countries.getValue() != null) {
            Collections.sort(countries.getValue(), (c1, c2) -> {
                if (c1.getPopulation() > c2.getPopulation()) {
                    return -1;
                } else if (c1.getPopulation() < c2.getPopulation()){
                    return 1;
                }
                return 0;
            });
            countries.setValue(countries.getValue());
        }
    }

    // Parsiranje preuzetih podataka i kreiranje adaptera za prikaz
    private void showData(List<Country> countries) {

        if (countries != null) {
            adapter = new CountryListAdapter(countries, requireActivity (), mainViewModel);
            recyclerView.setAdapter(adapter);
        } else {
            lableNoData.setVisibility(View.VISIBLE);
        }

    }

}
