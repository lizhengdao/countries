package com.example.projekat;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.projekat.ui.search.SearchViewModel;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private NavController navController;
    private MainViewModel mainViewModel;
    private TextView labelEmail;
    private TextView labelUsername;
    private View header;
    private SearchViewModel searchViewModel;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private SearchView searchView;
    private ImageView imageAvatarProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // Init komponenata
        initComponents();

        // Init pretrage
        initSearch();

        //Init navigacije
        initNavigacije();

        // Postavljanje username i email u drawer
        header = navigationView.getHeaderView(0);

        labelEmail = header.findViewById(R.id.nav_header_email);
        labelUsername = header.findViewById(R.id.nav_header_username);
        imageAvatarProfile = header.findViewById(R.id.nav_header_avatar);

        //Sakriti toolbar i drawer na login i register stranici
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if(destination.getId() == R.id.loginFragment || destination.getId() == R.id.registerFragment) {
                    toolbar.setVisibility(View.GONE);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                } else if (destination.getId() != R.id.nav_home) {
                    searchView.setVisibility(View.GONE);
                } else {
                    toolbar.setVisibility(View.VISIBLE);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    searchView.setVisibility(View.VISIBLE);
                }
            }
        });



        //Ne cuva se lokalno nista
        // Prilikom prvog aktiviranja

        mainViewModel.getUser().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser != null) {
//                    mainViewModel.getAppState().setValue(AppState.USER_AUTHENTICATED);
                    // Postavi korisnicko ime i email u drawer
                    labelUsername.setText(firebaseUser.getDisplayName());
                    labelEmail.setText(firebaseUser.getEmail());

                    if (firebaseUser.getPhotoUrl() != null) {
                        Picasso.get().load(firebaseUser.getPhotoUrl()).into(imageAvatarProfile);
                    } else {
                        imageAvatarProfile.setImageResource(R.mipmap.ic_launcher);
                    }
                } else {
                    navController.navigate(R.id.loginFragment);
                }
            }
        });

        // Slusa kada se drzava klikne i postavlja title na action bar te drzave
        mainViewModel.getCountryName().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                getSupportActionBar().setTitle(mainViewModel.getCountryName().getValue());
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void initComponents() {
        // Main viewmodel drzi stanje aplikacije
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // Pretraga
        searchViewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        // Postavljanje toolbara
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    private void initNavigacije() {
        // Uzimanje drawer i navigacije
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Konfigurisanje destinacije navigacije
        // Svaki id se smatra top level destinacijom dakle nema back arrow
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.favoritesFragment, R.id.settingsFragment, R.id.registerFragment, R.id.loginFragment)
                .setDrawerLayout(drawer)
                .build();
        // Povezivanje navigacije
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    private void initSearch() {
        searchView = toolbar.findViewById(R.id.search);

        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Search countries");

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                    searchView.setIconified(true);
                }
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewModel.doSearch(query);
                navController.navigate(R.id.searchFragment);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

}
