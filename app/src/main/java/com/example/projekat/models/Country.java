package com.example.projekat.models;

import java.util.List;

public class Country {

    private String name;
    private String capital;
    private long population;
    private String flag;
    private List<String> languages;
    private String region;
    private String numericCode;
    private List<Currency> currencies;
    private double area;

    public Country(String name, String capital, long population, String flag,
                   List<String> languages, String region, String numericCode,
                   List<Currency> currencies, double area) {
        this.name = name;
        this.capital = capital;
        this.population = population;
        this.flag = flag;
        this.languages = languages;
        this.region = region;
        this.numericCode = numericCode;
        this.currencies = currencies;
        this.area = area;
    }

    public Country() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public String getRegion() {
        return region;
    }

    public String getNumericCode() {
        return numericCode;
    }

    public void setNumericCode(String numericCode) {
        this.numericCode = numericCode;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }
}
