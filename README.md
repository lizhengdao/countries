## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)

## General info
This project is an Android application that shows countries and their details. Users must authenticate before using the application and they can save their favorite countries.
	
## Technologies
Project is created with:
* Android
* Firebase